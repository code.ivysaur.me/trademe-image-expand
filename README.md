# trademe-image-expand

![](https://img.shields.io/badge/written%20in-Javascript-blue)

A bookmarklet to enhance the website trademe.co.nz.

Replaces null-images on search results with loading the image from the auction listing page.

Tags: browser-extension

<html>
<a href="javascript:$(&quot;img[src$=hasPhoto_160x120\\.png]&quot;).each(function(){var e=$(this),t=e.parent().attr(&quot;href&quot;);e.css({height:&quot;120px&quot;,width:&quot;160px&quot;});$.get(t,function(t){var n=$(t).find(&quot;#mainImage&quot;);if(!n.length)return;e.attr(&quot;src&quot;,n.attr(&quot;src&quot;))})})">Drag to bookmarks bar</a>
</html>


## Download

- [⬇️ trademe.js.zip](dist-archive/trademe.js.zip) *(498B)*
